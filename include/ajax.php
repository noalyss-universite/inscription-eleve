<?php
/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
// Copyright (2016) Author Dany De Bontridder <dany@alchimerys.be>

require_once '../config.inc.php';
require_once NOALYSS_INCLUDE.'/lib/html_input.class.php';
require_once NOALYSS_INCLUDE.'/lib/ac_common.php';
require_once NOALYSS_INCLUDE.'/class/database.class.php';
require_once NOALYSS_INCLUDE.'/class/dossier.class.php';
require_once NOALYSS_INCLUDE.'/class/noalyss_user.class.php';

function send_xml($p_status,$p_string) {
    echo "$p_status";
    echo h($p_string);
}

$email=HtmlInput::default_value_post('ins_email','');
$name=HtmlInput::default_value_post('ins_name','');
$first_name=HtmlInput::default_value_post('ins_first_name','');
$tag=HtmlInput::default_value_post('ins_tag','-1');
$password=rand(0,99).rand(0,99).rand(0,99);



//------------------------------------------------
// Check parameters
//------------------------------------------------
if (  $name == '' || $first_name==''|| $tag == '-1') {
    send_xml('NOK ','Paramètre invalide');
    return;
}

//--------------------------------------------------
// Email not mandatory for mode MODE_INSCRIPTION
//--------------------------------------------------
if ( $email == '' && MODE_INSCRIPTION=='INSCRIPTION_ETUDIANT') {
    send_xml('NOK ','Paramètre invalide');
    return;
}
//--------------------------------------------------
// For Inscription by teacher an login is generated
//--------------------------------------------------
if ( MODE_INSCRIPTION == 'CREATION_DOSSIER') {
    $login=strtolower(substr($name.$first_name, 0, 5).rand (1000,9999));
    
} else {
    $login=strtolower($email);
}
$email=strtolower($email);
$cn=new Database();
/*
 * Record the input
 */
require_once INSCRIPTION.'/include/class_inscription_sql.php';
$record=new Inscription_SQL($cn);
$record->ins_ip_add = $_SERVER['REMOTE_ADDR'];
$record->ins_email = $email;
$record->ins_name=$name;
$record->ins_first_name=$first_name;
$record->ins_tag=$tag;
$record->ins_flag_result='P';

$record->save();
/*
 * Check user is not already created
 */
$exists=$cn->get_value('select count(*) from ac_users where use_login=$1',array($login));
if ( $exists > 0 ) {
    $record->ins_flag_result='E';
    $record->ins_result_process='Utilisateur déjà inscrit'.'<br/>';
    $record->ins_date_last_result=date ('d.m.Y H:i:s');
    $record->save();
    send_xml('NOK','Utilisateur déjà inscrit');
    return;
}

$user=new Noalyss_User($cn,0);
$user->login=$login;
$user->first_name=$first_name;
$user->last_name=$name;
$user->email=strtolower($email);
$user->password=md5($password);
$user->insert();

$record->ins_result_process= date ('d.m.y H:i:s').' utilisateur créé '.$password.' ok';
$record->use_id=$user->id;
$record->save();
/*
 * get template thanks tag
 */
$mod_id=$cn->get_value(' select mod_id from extended_admin.modele_tag where tag_id=$1',
        array($tag));

if ( $mod_id == '') {
    $record->ins_result_process.=date ('d.m.y H:i:s').' étiquette sans modèle'.'<br/>';
    $record->ins_flag_result='E';
    $record->ins_date_last_result=date ('d.m.Y H:i:s');
    $record->save();
    send_xml('NOK','Etiquette sans modèle');
    return;
}
$record->mod_id=$mod_id;
$record->save();
$tag_code=$cn->get_value('select tag_code from extended_admin.tag where id=$1',array($tag));
/*
 * Create database
 */
$template_name = domaine.'mod'.$mod_id;
$dos_id=$cn->get_next_seq('dossier_id');
$dos_name=" $dos_id $name";
$dos_description=" $email $tag_code";
$dossier_name=domaine.'dossier'.$dos_id;

$cn->exec_sql("create database {$dossier_name} encoding 'UTF8' template {$template_name} ");
$record->dos_id=$dos_id;
$record->ins_result_process.=date ('d.m.Y H:i:s').' base de donnée crée'.'<br/>';
$record->save();

$cn->exec_sql(' insert into ac_dossier(dos_id,dos_name,dos_description) values ($1,$2,$3)',array(
    $dos_id,
    $dos_name,
    $dos_description
));

// Save into extended_admin
$cn->exec_sql('insert into extended_admin.dossier_tag(dos_id,tag_id) values ($1,$2)',
        array($dos_id,$tag));

// set the periode
$year=date('Y');
$dossier_cn=new Database($dos_id);
$dossier_cn->exec_sql("delete from parm_periode");
if ( ($year % 4 == 0 && $year % 100 != 0) || $year % 400 == 0 )
    $fev=29;
else
    $fev=28;

$dossier_cn->exec_sql("delete from user_local_pref where parameter_type='PERIODE'");
$nb_day=array(31,$fev,31,30,31,30,31,31,30,31,30,31);
                $m=1;
foreach ($nb_day as $day)
{
    $p_start=sprintf("01-%d-%s",$m,$year);
    $p_end=sprintf("%d-%d-%s",$day,$m,$year);
    $sql=sprintf("insert into parm_periode (p_start,p_end,p_exercice)
    values (to_date('%s','DD-MM-YYYY'),to_date('%s','DD-MM-YYYY'),'%s')",
    $p_start,$p_end,$year);
    $Res=$dossier_cn->exec_sql($sql);
    $m++;
}
$sql="  insert into jrn_periode(p_id,jrn_def_id,status) ".
"select p_id,jrn_def_id, 'OP'".
" from parm_periode cross join jrn_def";
$dossier_cn->exec_sql($sql);
Dossier::synchro_admin($dos_id);
$record->ins_result_process.=date ('d.m.y H:i:s').' Paramètrage ok'.'<br/>';
/*
 * Create user give him the administrator profile
 */
$cn->exec_sql("insert into jnt_use_dos(use_id,dos_id) values ($1,$2)",
        array($user->id,$dos_id));
$dossier_cn->exec_sql('insert into profile_user(user_name,p_id) values($1,1)',
        array($user->login));
// Grant all action + ledger to him
$dossier_cn->exec_sql("insert into user_sec_act (ua_login,ua_act_id) select '{$user->login}',ac_id from action");
$dossier_cn->exec_sql("insert into user_sec_jrn(uj_login,uj_jrn_id,uj_priv) select '{$user->login}',jrn_def_id,'W' from jrn_def");
        
/*
 * grant teacher with administrator profile
 */
$a_teacher = $cn->get_array("select use_id,use_login,use_admin
    from extended_admin.teacher_tag
    join ac_users using (use_id)
    where tag_id=$1",array($tag));
$nb_teacher=count($a_teacher);
for ($i=0;$i< $nb_teacher;$i++) {
    //------
    // Do not grant ADMINISTRATOR
    //--------
    if ( $a_teacher[$i]['use_admin'] == 0) {
        $cn->exec_sql("insert into jnt_use_dos(use_id,dos_id) values ($1,$2)",
                array($a_teacher[$i]['use_id'],$dos_id));
    }
    //------------------
    // if exist do not reinsert them
    //-------------------
    if ( $dossier_cn->get_value('select count(*) from profile_user where user_name = $1',array($a_teacher[$i]['use_login'])) == 0 )
    {
        $dossier_cn->exec_sql('insert into profile_user(user_name,p_id) values($1,1)',
                array($a_teacher[$i]['use_login']));
        // Grant all action + ledger to him
        $dossier_cn->exec_sql("insert into user_sec_act (ua_login,ua_act_id)"
                . " select $1,ac_id from action",array($a_teacher[$i]['use_login']));

        $dossier_cn->exec_sql("insert into user_sec_jrn(uj_login,uj_jrn_id,uj_priv)"
                . " select $1,jrn_def_id,'W' from jrn_def",
                        array($a_teacher[$i]['use_login']));
    }
}
$record->ins_result_process.=date ('d.m.y H:i:s').' Accès autorisé'.'<br/>';
$record->save();
//----------------------------------------------------
// with MODE_INSCRIPTION = INSCRIPTION_ETUDIANT , the student 
// receives an email with connection
//
//----------------------------------------------------
if ( MODE_INSCRIPTION == 'INSCRIPTION_ETUDIANT')
{
    $headers = 'From: NOALYSS <'.EMAIL_SENDER.'>' . "\r\n";
    $r=mail ($email,'Base de donnée crée',
            "Bonjour, "."\r\n"
            ." votre dossier a été crée , "
            . " Votre nom d'utilisateur est {$email}"
            . " , votre mot de passe est {$password}");
    $record->ins_date_last_result=date ('d.m.Y H:i:s');        
    if ( $r) {        
        $record->ins_result_process.=date ('d.m.y H:i:s').' Envoi email'.'<br/>';
        $record->ins_flag_result='S';
        $record->save();
    }else {
        $record->ins_result_process.=date ('d.m.y H:i:s').' Envoi email echec'.'<br/>';
        $record->ins_flag_result='E';
        $record->save();
    }

    ?>
    <h1>Inscription réussie </h1>
    <p>
        Vous devriez recevoir un courriel venant de <?php echo EMAIL_SENDER;?> dans
        votre boîte au lettres électronique.
    </p>
    <p>
        Ce courriel contient votre mot de passe.
    </p>
    <p>
        Bonne journée,
    </p>
<?php
    return;
}
//----------------------------------------------------
// with MODE_INSCRIPTION = INSCRIPTION_ETUDIANT , the student 
// receives an email with connection
//
//----------------------------------------------------
if ( MODE_INSCRIPTION == 'CREATION_DOSSIER')
{
        ?>
    <h1>Dossier crée</h1>
    <ul>
        <li>
            Nom = <?php echo $first_name." ".$name ?>
        </li>
        <li>
            Login = <?php echo $login?>
        </li>
        
        <li>
            Mot de passe = <?php echo $password;?>
        </li>
    </ul>
    <p>
        <a class="button" href="?index.php"> Nouveau dossier</a>
    </p>
    </p>
<?php
    
}
