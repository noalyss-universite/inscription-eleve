<?php
/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
// Copyright (2016) Author Dany De Bontridder <dany@alchimerys.be>

if (!defined('ALLOWED'))
{
    die('Appel indirect interdit');
}
$cn=new Database();
?>
<div style="width:80%;margin-left:10%">
    <h1><?php echo ECOLE;?></h1>
    <div id="inscription_div" style="width:60%;margin-left:20%">
    <p>
        Enregistrement de votre dossier comptable.
    </p>
    <form method="post" id="inscription_frm" onsubmit="send_inscription();return false;">
        <table>
            <tr>
                <td>
                    <label id='label_ins_name' for="ins_name">Nom</label>
                </td>
                <td>
                    <input type="text" id="ins_name" name="ins_name" autofocus="on" placeholder="Nom">
                </td>
            </tr>
            <tr>
                <td>
                    <label  id='label_ins_first_name'  for="ins_first_name">Prénom</label>
                </td>
                <td>
                    <input type="text" id="ins_first_name" name="ins_first_name" placeholder="Prénom">
                </td>
            </tr>
            <tr>
                <td>
                    <label id='label_ins_email' for="ins_email">Adresse courriel</label>
                </td>
                <td>
                    <input type="email" id="ins_email" name="ins_email" placeholder="prenom.nom@u-bordeaux.fr">
                </td>
            </tr>
            
            <tr>
                <td>
                    <label id='label_ins_tag' for="ins_tag">Etiquette</label>
                </td>
                <td>
                    <select name="ins_tag" id="ins_tag">
                        <?php
                            $array=$cn->get_array('select tag_id,tag_code
                                                    from
                                        extended_admin.modele_tag 
                                        join extended_admin.tag on (tag.id=tag_id)
                                        order by tag_code ');
                            $nb_array=count($array);
                            for ($i=0;$i<$nb_array;$i++) :
                            ?>
                        <option value="<?php echo $array[$i]['tag_id'];?>">
                            <?php echo $array[$i]['tag_code'];?>
                        </option>
                        <?php
                            endfor;
                        ?>
                    </select>
                </td>
            </tr>

        </table>
        <p style="text-align: center">
            <input type='hidden' name='action' value='inscription'>
        <input type="submit" value="Valider" >
            
        </p>
    </form>
    </div>
</div>
<script> 
function validate_inscript() {
<?php
if (MODE_INSCRIPTION=="CREATION_DOSSIER") {
?>
        $('ins_email').value=$('ins_name').value+$('ins_first_name').value+'@dossier';
<?php
}
?>
       var array = ['ins_email','ins_name','ins_first_name'];
    var i=0;
    for ( i=0;i < array.length;i++)
    {
        if ( $(array[i]).value.trim()=='') {
            $(array[i]).style.borderColor = 'red';
            $(array[i]).style.borderSize = '2px';
            $('label_'+array[i]).style.color='red';
            return false;
        } else {
            $(array[i]).style.borderColor = 'black';
            $(array[i]).style.borderSize = '1px';
            $('label_'+array[i]).style.color='';
        }
        
    }
    return true;
}
function send_inscription() {
 if ( ! validate_inscript() )return false;
    var param=$('inscription_frm').serialize(true);
<?php
    if (MODE_INSCRIPTION=="CREATION_DOSSIER") {
?>
        param['ins_email' ]=$('ins_name').value+$('ins_first_name').value+'@dossier';
<?php
}
?>
    $('inscription_div').innerHTML=' Création en cours , un instant '+'<img src="loading.gif"/>';
    param['action']='create_db';
    new Ajax.Request('index.php',{
        method:'post',
        parameters : param,
        onSuccess: function (req) {
            $('inscription_div').innerHTML=req.responseText;
    } });
    
}
</script>
