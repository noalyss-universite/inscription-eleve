<!DOCTYPE html>
<html>
    <head>
        <title>Inscription Université de Bordeau</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
         <LINK REL="stylesheet" type="text/css" href="inscription.css?v=1" media="screen"/>
         <script src="prototype.js"></script>
    </head>
    <body>
        
<?php
/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
// Copyright (2016) Author Dany De Bontridder <dany@alchimerys.be>

/**
 * Inscription des étudiants
 * 
 */
require_once '../config.inc.php';
if ( ! defined ("MODE_INSCRIPTION")) {
    define ("MODE_INSCRIPTION","INSCRIPTION_ETUDIANT");
}
printf ('<img id="bg" src="%s"></img>',BACKGROUND_IMAGE);
require_once NOALYSS_INCLUDE.'/constant.php';
require_once NOALYSS_INCLUDE.'/lib/html_input.class.php';
require_once NOALYSS_INCLUDE.'/lib/ac_common.php';
if ( !defined ("MODE_INSCRIPTION") ) {
    echo "Vous devez spécifier le MODE_INSCRIPTION dans le fichier config.inc.php",
            " , un exemple se trouve dans le fichier config.inc.example";
    return;
}



define ('ALLOWED',true);
$action=HtmlInput::default_value_post('action',"");


// Rien n'est demandé , on affiche le formulaire d'inscription
if ( $action == "" ) {
        include INSCRIPTION.'/include/inscription.php';
} elseif ( $action == 'create_db') {
    // Action inconnue 
    require_once INSCRIPTION.'/include/ajax.php';
}
?>
   
    </body>
</html>
